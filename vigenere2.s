.data

affichage_erreur:
	.string "usage -> vigenere2 <clef> <'message'>"

chaine_de_caractere:
	.string ""
	
.text
.global _start

# ---------------------------------------------
#  Initialisation départ
# ---------------------------------------------
_start:

	# Verification des arguments sur la ligne de commande 
	pop %rax
	cmp $3, %rax
	jne affichage_erreur_argument		
	xor %rax, %rax

	# Récupération du nom du programme, la clef et le message
	pop %r13
	pop %r13
	pop %r14

	# Copie de la clé dans r15 pour l'utiliser plus tard
	mov %r13, %r15

	# On initialise notre chaine de caractere dans r12
	mov $chaine_de_caractere, %r12

	# Initialisation d'une variable qui permet de savoir s'il faut
	# mettre un point quand il y a une répétition de mauvais caracteres (w dans notre l'algorithme)
	xor %r8, %r8
	mov $1, %r8

# ---------------------------------------------
#  Boucle principale (tant que mot non vide)
# ---------------------------------------------
boucle_tant_que:

	# Récupération du premier caractère du message dans al
	# Vérification que le message ne soit pas vide
	# Si le message est vide on saute dans fin tant que
	movb (%r14),%al
	test %al, %al
	jz fin_boucle_tant_que

# ---------------------------------------------
#  Test de la lettre
# ---------------------------------------------

	# On verifie que la lettre se trouve bien entre
	# le code ascii 122 et 97 pour que ce soit une minuscule
	cmp $122, %rax
	jg lettre_non_alphabet
	cmp $97, %rax
	jl verification_majuscule
	
	# si c'est une minuscule alors w = 1
	mov $1, %r8	
	jmp algorithme_clef_et_calcul_caractere

# ---------------------------------------------
#  Test si la lettre est une majuscule
# ---------------------------------------------
verification_majuscule:

	# Si le caractere possede le code ascii
	# Inférieur à 97 on va regarder s'il est une majuscule
	# donc si plus petit que 65 c'est pas une majuscule
	# si plus grand que 90 c'est pas une majuscule non plus
	# si c'est une majuscule on lui ajoute + 32 pour le transformer en minuscule
	cmp $65, %rax
	jl lettre_non_alphabet
	cmp $90, %rax
	jg lettre_non_alphabet
	add $32, %rax
	

# ---------------------------------------------
#  Récuperation + test de la clef + calcul du caractere
# ---------------------------------------------
algorithme_clef_et_calcul_caractere:

	# Récuperation du caractere de la clef
	# si la clef n'est pas vide on va dans la suite du programme
	# sinon on remet dans le registre de la clef
	# la sauvegarde qu'on avait fais en début de programme
	movb (%r13), %bl
	test %bl, %bl
	je reset_de_la_clef

	# Effectue les calculs pour obtenir le décalage
	# pour le code de vigenère
	# on oublie pas d'augmenter r13 pour le caractère suivant
	sub $97, %al
	add %al, %bl # Dans rbx se trouve le caractere a afficher suite au calcul
	inc %r13
	jmp verification_depassement

reset_de_la_clef:
	mov %r15, %r13 # reset de la clef
	jmp algorithme_clef_et_calcul_caractere

# ---------------------------------------------
#  Calcul de la taille du caractere pour le recalibrer
# ---------------------------------------------
verification_depassement:

	# Vérification que le caractère est plus petit ou egal 
	# au code ascii 122
	cmp $122, %rbx

	# si le caractere est plus petit ou egal on passe à l'étape suivante
	jle fin_algorithme_depassement

	# Retire 26 du caractere et retourne tester dans la verification_depassement
	sub $26,%bl
	jmp verification_depassement


fin_algorithme_depassement:	

	# On met le caractere dans la chaine de caractere qui se trouve dans r12
	# On incrémente la chaine de caractere
	# On incrémente le message
	# On incrémente le compteur de la taille du message
	movb %bl, (%r12)
	inc %r12
	inc %r14
	inc %rdx
	jmp boucle_tant_que


# ---------------------------------------------
#  Cas d'une lettre non dans l'alphabet
# ---------------------------------------------
lettre_non_alphabet:

	# Cas d'une lettre qui n'est pas représentable
	# dans le code de vigenere
	cmp $1, %r8 	# si w = 1 on met un point
	je on_met_un_point # Si c'est pas égale on ne met pas de "."
	jmp suite_du_point

on_met_un_point:
	mov $46, %rbx	# code ascii du point
	movb %bl, (%r12)	# on met le point dans la chaine de caractere
	inc %r12 	# Augmente la taille de la chaine de caractere
	xor %r8, %r8	# w = 0
	inc %rdx	# Augmente le compteur de la taille du message

suite_du_point:

	# On incrémente le message pour le caractere suivant
	# Retour dans la boucle tant que
	inc %r14
	jmp boucle_tant_que
	
	
# ---------------------------------------------
#  Affichage de fin
# ---------------------------------------------
affichage_erreur_argument:

	mov $1, %rax			
	mov $1, %rdi
	mov $affichage_erreur, %rsi	# string du message
	mov $37, %rdx # taille du message
	syscall

	# Saut de ligne
	push $10	# Saut de ligne
	mov %rsp,%rsi		# affiche le message
	movq $1, %rax 		# rax <- 1 (numero de write)
	movq $1, %rdi 		# rdi <- 1 (stdout)
	movq $1, %rdx 		# rdx <- 1 (longueur chaine)
	syscall
	
	mov $60, %rax
	xor %rdi, %rdi
	syscall

fin_boucle_tant_que:

	# Affichage du contenue de la chaine de caractere
	# avec dans rdx la taille à afficher qui augmentait tout le long du programme
	mov $1, %rax
	mov $1, %rdi
	mov $chaine_de_caractere, %rsi	# resultat de l'algorithme dans la chaine de caractere
	syscall

	# Saut de ligne
	push $10	# Saut de ligne
	mov %rsp,%rsi		# affiche le message
	movq $1, %rax 		# rax <- 1 (numero de write)
	movq $1, %rdi 		# rdi <- 1 (stdout)
	movq $1, %rdx 		# rdx <- 1 (longueur chaine)
	syscall
	
	# Quitter le programme
	mov $60, %rax
	xor %rdi, %rdi
	syscall